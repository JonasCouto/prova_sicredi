// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })



Cypress.Commands.add('selectSwitchVersion', (version) => { 
    cy.get('#switch-version-select')
    .select(version)
})

Cypress.Commands.add('addRecord', (user) => {
    
    cy.fixture(user)
    .then((user) => {

        cy.get('#field-customerName')
        .should('be.visible')
        .type(user.name)
        .should('have.value', user.name)

        cy.get('#field-contactLastName')
        .should('be.visible')
        .type(user.lastName)
        .should('have.value', user.lastName)

        cy.get('#field-contactFirstName')
        .should('be.visible')
        .type(user.contactFirstName)
        .should('have.value', user.contactFirstName)

        cy.get('#field-phone')
        .should('be.visible')
        .type(user.phone)
        .should('have.value', user.phone)

        cy.get('#field-addressLine1')
        .should('be.visible')
        .type(user.addressLine1)
        .should('have.value', user.addressLine1)

        cy.get('#field-addressLine2')
        .should('be.visible')
        .type(user.addressLine2)
        .should('have.value', user.addressLine2)
        
        cy.get('#field-city')
        .should('be.visible')
        .type(user.city)
        .should('have.value', user.city)

        cy.get('#field-state')
        .should('be.visible')
        .type(user.state)
        .should('have.value', user.state)

        cy.get('#field-postalCode')
        .should('be.visible')
        .type(user.postalCode)
        .should('have.value', user.postalCode)

        cy.get('#field-country')
        .should('be.visible')
        .type(user.country)
        .should('have.value', user.country)

        cy.get('#field-salesRepEmployeeNumber')
        .should('be.visible')
        .type(user.fromEmployer)

        cy.get('#field-creditLimit')
        .should('be.visible')
        .type(user.creditLimit)
        .should('have.value', user.creditLimit)  
    })
    
    cy.get('#form-button-save')
    .click() 
           

})
