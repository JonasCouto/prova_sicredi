/// <reference types="cypress" />


    describe('Prova técnica de Automação de teste', () => {
      
      beforeEach(() => {
        cy.visit(Cypress.config('baseUrl'))
      
      })
      
      it('Desafio 1', () => {
        
        cy.selectSwitchVersion('Bootstrap V4 Theme')

        // Clica no botão add Record
        cy.get('.floatL.t5 > .btn')
        .click()
        
        // Cria novo usuário
        cy.addRecord('user_sicredi.json')
        
        // Válida mensagem de usuário criado com sucesso
        cy.get('#report-success')
        .should('have.text', 'Your data has been successfully stored into the database. Edit Record or Go back to list')
        .should('be.visible')

      })

      it('Desafio 2', () => {
        
        cy.selectSwitchVersion('Bootstrap V4 Theme')
        
        // Clica no botão add Record 
        cy.get('.floatL.t5 > .btn')
        .click()

        // Cria novo usuário
        cy.addRecord('user_sicredi.json')
          

        // Válida mensagem de usuário criado com sucesso
        cy.get('#report-success')
        .should('have.text', 'Your data has been successfully stored into the database. Edit Record or Go back to list')
        .should('be.visible')

        // Clica no botão de salvar e voltar 
         cy.get('#save-and-go-back-button')
        .click()
         
        //Intercepta a chamada de filtragem por usuários 
        cy.intercept('POST','**/ajax_list').as('lista')

        //Filtra pelo nome do usuário
        cy.get(':nth-child(3) > .form-control')
        .click()
        .should('be.visible')
        .type('Teste Sicredi')
        .should('have.value', 'Teste Sicredi' )

        // Espera carregar a lista de usuários
        cy.wait('@lista')
        
        // Seleciona usuário
        cy.get('.select-all-none')
        .check()

        // Clica no botão de deletar
        cy.get('.no-border-left > .floatL > .btn')
        .click()

        // Verifica mensagem de aviso para deletar
        cy.get('.alert-delete-multiple-one') 
        .contains('Are you sure that you want to delete')
       
        // Delata usuário
        cy.get('.delete-multiple-confirmation > .modal-dialog > .modal-content > .modal-footer > .btn-danger')
        .click()

        // Verifica mensagem de usuário deletado
        cy.get('[data-growl="message"] > p')
        .should('have.text', 'Your data has been successfully deleted from the database.')

      })  
    
})
    
    
    
    
    
    
    
    
    